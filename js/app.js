

// CounterUp
$('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
});

/* Go up */
$(window).scroll(function () {
    if($(this).scrollTop() > 100 ) {
        $(".go__up").css("right","20px");
    }else {
        $(".go__up").css("right","-60px");
    }
});

$(".go__up").click(function(){
    $("html,body").animate({scrollTop:0},500);
    return false;
});

baguetteBox.run('.project');

$('.testimonial').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    dots: true,
    autoplaySpeed: 3000,
    responsive: [
        {
        breakpoint: 1024,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
        },
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
        },
        {
        breakpoint: 567,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
        }
    ]
});
$('.client__img').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    dots: false,
    autoplaySpeed: 3000,
    responsive: [
        {
        breakpoint: 1024,
        settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
        }
        },
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1
        }
        },
        {
        breakpoint: 567,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1
        }
        }
    ]
});

$("form[name='registration']").validate({
    rules: {
        name: "required",
        subject: "required",
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        name: "Please enter your name",
        subject: "Please enter subject name",
        email: "Please enter a valid email address"
      },
      submitHandler: function(form) {
        form.submit();
      }
});